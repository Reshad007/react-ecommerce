import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import logo from '../logo.svg';
import styled from 'styled-components';
import { Button } from './Button';

class Navbar extends Component {
	render() {
		return (
			<Nav className="navbar navbar-expand-sm navbar-dark px-sm-5">
				<NavLink to="/">
					<img className="navbar-brand" src={logo} alt="" />
				</NavLink>
				<ul className="navbar-nav align-items-center">
					<li className="nav-item ml-5">
						<NavLink to="/" className="nav-link">
							products
						</NavLink>
					</li>
				</ul>
				<NavLink to="/cart" className="ml-auto">
					<Button>
						<span className="mr-2">
							<i className="fas fa-cart-plus" />
						</span>
						cart
					</Button>
				</NavLink>
			</Nav>
		);
	}
}

const Nav = styled.nav`
	background-color: var(--mainBlue);
	.nav-link {
		color: var(--mainWhite) !important;
		font-size: 1.3rem;
		text-transform: capitalize;
	}
`;

export default Navbar;
